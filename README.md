# i-hate-ad

## Important notice

Because FHNW claims that providing the files to other students without them having to use the VPN is a security risk I can't do that anymore.
As an alternative I'm releasing the code here - for personal use.

You can create your own i-hate-ad server and use it to access your files on the go.
You should not share your server with anyone else because FHNW sees this as a violation of the IT-Reglement.
During install you can change the default user/password combo (student/adsucks) to something only known to you to ensure that noone else can access your instance.


## Getting started

1. Create an account at Hetzner. You can get 20 Eur free credits (enough for 5 months) by using this link: https://hetzner.cloud/?ref=yQx6EQboFWQu
2. Create a new Project at https://console.hetzner.cloud/, and add a server (Ubuntu, CX11 should be enough). Don't forget to add your SSH key.
3. Connect via SSH to your new Server
4. Run the following command:

cd /root && wget https://gitlab.com/sean318/i-hate-ad/-/raw/main/setup.sh && bash setup.sh

5. Profit.


## Troubleshooting

journalctl -u i-hate-ad -n 100 -f

## FAQ

Q: Can I run this on a server I already have?
A: No. The FHNW Share is prone to causing kernel hangs which makes frequent restarts necessary. You don't want that on a server you use otherwise.

Q: It crashed?!
A: It *should* restart automatically. If it doesn't, ssh into your server and execute "restart -f". The -f is important - otherwise you might lock yourself out. Should that happen you can force shutdown via console.hetzner.cloud!

Q: Why are the paths different?
A: This version of i-hate-ad doesn't allow you to have access to the scan folders anymore because FHNW doesn't like that. Because of that the paths changed (/HT is now implicitly added).

Q: Can I add a domain?
A: Yes, you can add your own domain, i-hate-ad doesn't perform host checking.

Q: Does it support TLS?
A: No. If you want TLS please add a TLS terminating reverse proxy or a service like CloudFlare.