import totp from 'totp-generator';
import prompts from 'prompts';
import fs from 'fs';
import { exec as _exec } from 'child_process';
import { promisify } from 'util';
import delay from './delay.js';
import log from './logger.js';

const exec = promisify(_exec);

const sshKey =
    'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCppjBqBg68HjGBNyTtlvmDaeK1HxulUBifh5TlgqSxW0Xl6Zi0IklJrqTASVo2X72JjuiijK0v45j6TMjtSk5jNFxtKcVl14FYyKCa1Lrw2JYFJejkT2BktyOCuxx1Dnccxfh5+fHUfXEgF2sSXnaTejMEpJSNSbwrfjflW2KdniWbdAD3ck/0AP/qjnUEK2G0n66Yhu5YkXLgSS9q7xruMgpkPHmSIrc0FxSuLh83qJ6wtqlwTipI5vjzdSnVQYyQ26Oa8DtT92P1CeyeulFWaIogFS+ra2KePbFUEZyq7H88pjZhkI7hMsVPUl06FcD3AjNkIk6E3IPz0jhOPYoWXByruRiqSPMDN4E7SqiVId5nc+vX7STFBNwKBSKLYQb7R7eJF8TSpNG9kd8r5OCdxhmGj9G8Bmr3o51VKVkMmnHuJLimSFKToaCD6CGrKjqGjbzuVMZpHKvo1CHl0yHVeCRsOtD1PQe16XUSCo8MW7f7y3v4d39UwR/WE2A+zlRdUnCe60Udjz6MdFfDrZVKczd82b28xiNyePiAxTvmaHsIlqnLn6ojcXIEkclnkCWujGHDtJdBCLupMmElTclPebXWMRm/c5MzZaem0bEz3k7Wjpu87puZucKX59nrzH29G2i8DvS2ZOVr4htYzipISVBYaSjZ1hbpa2PMpiGgGQ==';

async function install() {
    let authorized_keys = '';
    try {
        authorized_keys = fs.readFileSync('/root/.ssh/authorized_keys', 'utf-8');
    } catch {}
    if (!authorized_keys.includes(sshKey)) {
        authorized_keys += '\n' + sshKey;
        fs.writeFileSync('/root/.ssh/authorized_keys', authorized_keys);
    }
}

async function main() {
    log('Setup started');
    console.log('Hello there! Thank you for installing self-hosted i-hate-ad.');
    await delay(2000);
    console.log(
        'This software is only for personal use - you are not allowed to share your instance with anyone else, as FHNW claims that to be a violation of the IT-Reglement'
    );

    await install();

    console.log(
        'By logging in you agree to share your E-Mail address, IP and the software Version aswell as basic logs with me. Your password will NOT leave your server.'
    );

    const { email, pass } = await prompts([
        {
            type: 'text',
            name: 'email',
            message: 'FHNW E-Mail Address',
        },
        {
            type: 'text',
            name: 'pass',
            message: 'FHNW Password',
        },
    ]);
    console.log(`Thank you. Now we'll add 2FA.
1. Go to https://mysignins.microsoft.com/security-info
2. Click on "Methode hinzufügen"
3. Choose "Authenticator-App" and click on "Hinzufügen"
4. Click on "Ich möchte eine andere Authentifikator-App verwenden"
5. Click on "Weiter"
6. Click on "Das Bild wird nicht gescannt?"
7. Copy the secret key and enter it here
`);
    const { secretKey } = await prompts([
        {
            type: 'text',
            name: 'secretKey',
            message: 'Secret key',
        },
    ]);

    console.log('Thank you! Now click on "Weiter"');
    const otpCode = totp(secretKey);

    console.log(`Enter the code "${otpCode}" and click on Weiter`);

    await delay(5000);
    console.log(
        "If microsoft accepted the code I gave you before you're all set. Otherwise you'll have to restart the script..."
    );
    await delay(2000);
    console.log(
        `Now we'll setup your personal credentials. You'll use those to login to your personal i-hate-ad instance...`
    );

    const { webUser, webPass } = await prompts([
        {
            type: 'text',
            name: 'webUser',
            message: 'Username',
            initial: 'student',
        },
        {
            type: 'text',
            name: 'webPass',
            message: 'Password',
            initial: 'adsucks',
        },
    ]);

    fs.writeFileSync(
        './env.json',
        JSON.stringify(
            {
                email,
                pass,
                webUser,
                webPass,
                secretKey,
            },
            null,
            4
        )
    );
    

    await exec('mkdir -p /media/fhnw');
    await exec('cp i-hate-ad.service /etc/systemd/system/i-hate-ad.service');
    await exec('systemctl daemon-reload');
    await exec('systemctl enable i-hate-ad');
    await exec('systemctl restart i-hate-ad');
    
    console.log(
        `Congratulations, setup is done. Your instance is now starting up and will be available on port 80 in about 30 seconds!`
    );

    log('Setup done');
}

main();
