import { exec as _exec } from 'child_process';
import { promisify } from 'util';
const exec = promisify(_exec);

import login from './login.js';
import delay from './delay.js';
import log from './logger.js';
import fs from 'fs';

const config = JSON.parse(fs.readFileSync('./env.json'));


async function main() {
    log('starting');
    console.log(`Applying kernel settings`);
    await exec('sysctl -w kernel.panic=5');
    await exec('sysctl -w kernel.hung_task_panic=1');
    await exec('sysctl -w kernel.hung_task_timeout_secs=30');
    console.log(`Getting token for ${config.email}`);
    const vpnToken = await login(config);
    console.log(`Got token. Connecting to VPN...`);

    const vpn = exec(
        `./openconnect vpn1.fhnw.ch --useragent="AnyConnect Darwin_i386 4.10.00093" --token-mode="anyconnect-sso"  -s 'vpn-slice 10.0.0.0/8' --token-secret=${vpnToken}`
    , {
        env: {
            LD_LIBRARY_PATH: '/root/i-hate-ad/.libs'
        }
    });
    await delay(10000);
    console.log('mounting share');
    await exec('resolvectl dns tun0 10.51.2.228');
    await exec(
        `mount -t cifs //fs.edu.ds.fhnw.ch/data /media/fhnw -o username=${config.email},password=${config.pass},iocharset=utf8`
    );
    import('./server.js');
    log('running');
}


async function init(){
    try{
        await main();
    }catch{
        exec('reboot -f');
    }
}

init();
