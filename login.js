import puppeteer from 'puppeteer';
import totp from 'totp-generator';
import delay from './delay.js';

async function login({ email, pass, secretKey }) {
    const browser = await puppeteer.launch({args: ['--no-sandbox']});
    const page = await browser.newPage();
    await page.goto(
        'https://vpn1.fhnw.ch/+CSCOE+/saml/sp/login?tgname=DefaultWEBVPNGroup&acsamlcap=v2'
    );
    await delay(1000);
    await page.waitForSelector('[name="loginfmt"]');
    await (await page.$('[name="loginfmt"]')).type(email);
    await delay(1000);

    await page.click('[type="submit"]');
    await delay(1000);

    await page.waitForSelector('[name="passwd"]');
    await (await page.$('[name="passwd"]')).type(pass);
    await delay(1000);

    await page.click('[type="submit"]');

    await delay(1000);

    await page.waitForSelector('[data-value="PhoneAppOTP"]');
    await (await page.$('[data-value="PhoneAppOTP"]')).click();

    await delay(1000);
    await page.waitForSelector('[name="otc"]');

    const otpCode = totp(secretKey);
    await (await page.$('[name="otc"]')).type(otpCode);
    await delay(1000);

    await page.click('[type="submit"]');

    await delay(1000);
    await page.click('#idBtn_Back');
    await page.waitForNavigation();
    await delay(500);

    const cookies = await page.cookies();

    return cookies.find((c) => c.name === 'acSamlv2Token').value;
}

export default login;
