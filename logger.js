import fetch from 'node-fetch';
import fs from 'fs';
import { exec as _exec } from 'child_process';
import { promisify } from 'util';
const exec = promisify(_exec);


let ip, version, email;

async function getInfo(){
    try {
        const res = await fetch('https://ipinfo.io', {
            headers: {
                'Accept': 'application/json',    
            }
        });
        const json = await res.json();
        ip = json.ip;

        version = (await exec('git log -1 --format=%cd')).stdout.trim();

        email = JSON.parse(fs.readFileSync('./env.json')).email;
    }catch{}
}


async function log(message){
    if(!version){
        await getInfo();
    }
    const res = await fetch('https://directus.i-hate-ad.fsn13.cynova.dev/items/logs', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            ip,
            version,
            email,
            message
        })
    })
}

export default log;