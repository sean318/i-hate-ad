import express from 'express'
const app = express();
import serveStatic from 'serve-static'
import basicAuth from 'express-basic-auth'
import fs from 'fs';
import path from 'path';
import { dirname } from 'path';
const rootDirectory = '/media/fhnw/HT'
import archiver from 'archiver';
import cookieParser from 'cookie-parser'
const config = JSON.parse(fs.readFileSync('./env.json'));
const __dirname = dirname('./')


const dirCache = {};
const maxAge = 60 * 1000;


app.use(cookieParser('fdjkshafkj4u98das'))

function auth(req, res, next) {
    if (!req.signedCookies.user) {
        if (req.query.token === `${config.webUser}_${config.webPass}`) {
            res.cookie('user', 'admin', {
                signed: true,
                maxAge: 365 * 24 * 60 * 60 * 1000
            });
            next();
            return;
        }


        var authHeader = req.headers.authorization;
        if (!authHeader) {
            var err = new Error("You are not authenticated");

            res.setHeader("WWW-Authenticate", "Basic");
            err.status = 401;
            next(err);
            return;
        }

        var auth = new Buffer.from(authHeader.split(" ")[1], "base64")
            .toString()
            .split(":");
        var username = auth[0];
        var password = auth[1];

        if (username == config.webUser && password === config.webPass) {
            res.cookie('user', 'admin', {
                signed: true,
                maxAge: 365 * 24 * 60 * 60 * 1000
            });
            next();
            return;
        } else {
            var err = new Error("You are not authenticated");

            res.setHeader("WWW-Authenticate", "Basic");
            err.status = 401;
            next(err);
            return;
        }
    } else {
        if (req.signedCookies.user == 'admin') {
            next();
        } else {
            var err = new Error("You are not authenticated");
            err.status = 401;
            next(err);
            return;
        }
    }
}



app.use(auth)

app.get('/api/ls/:path*?', (req, res) => {
    const dirName = path.resolve(rootDirectory, (req.params.path || '') + (req.params[0] || ''));
    if (dirName.indexOf(rootDirectory) !== 0) {
        res.send('No. Fuck off!');
        return;
    }
    let sent = false;
    if (dirCache[dirName]) {
        res.send(dirCache[dirName]);
        sent = true;
        if (Date.now() - dirCache[dirName].cached < maxAge) {
            return
        }
    }
    const startTime = Date.now();
    fs.readdir(dirName, { withFileTypes: true }, (err, list) => {
        if (err) {
            res.status(500).end(String(err))
        } else {
            const data = list.map(dirent => {
                try {
                    const stat = fs.statSync(path.join(dirName, dirent.name));
                    return {
                        name: dirent.name,
                        isDirectory: dirent.isDirectory(),
                        suffix: dirent.name.match(/.+\.(.+)/)?.[1]?.toLowerCase(),
                        cTime: Math.round(stat.ctimeMs),
                        mTime: Math.round(stat.mtimeMs),
                        size: stat.size
                    }
                } catch (e) {
                    return {
                        name: dirent.name,
                        isDirectory: dirent.isDirectory(),
                        suffix: dirent.name.match(/.+\.(.+)/)?.[1]?.toLowerCase(),
                    }
                }
            });
            dirCache[dirName] = {
                cached: Date.now(),
                timing: Date.now() - startTime,
                data,
            }
            if (!sent) {
                res.send(dirCache[dirName])
            }
        }
    })

})

app.get('/archive/:path*?', (req, res) => {
    const dirName = path.resolve(rootDirectory, (req.params.path || '') + (req.params[0] || ''));
    console.log(`archiving ${dirName}`);
    if (dirName.indexOf(rootDirectory) !== 0) {
        res.send('No. Fuck off!');
        return;
    }
    if (dirName.split('/').length < 6) {
        res.send('ARE YOU FOR REAL? You really think i\'d let you create an archive this high up in the tree? This would probably fuck up everything between your Browser and AD (inclusive).');
        return;
    }
    const folderName = dirName.split('/').pop();
    const archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });
    res.header('Content-Disposition', `attachment; filename="${folderName}.zip"`);
    archive.pipe(res)
    archive.directory(dirName, folderName)
    archive.finalize();
})


app.use('/files', express.static(rootDirectory, { maxAge: 1000 * 60 }));
app.use(express.static(path.resolve(__dirname, 'spa'), { maxAge: 1000 * 60, index: 'index.html' }));

app.get('*', function (req, res) {
    res.sendFile(path.resolve(__dirname, 'spa/index.html'));
});


app.listen(80);